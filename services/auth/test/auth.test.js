require('../app')
const chai = require('chai')
const chaiHttp = require('chai-http')
const { ResponseCode } = require('../app/core/lib')

const { expect } = chai
chai.use(chaiHttp)

const { URL, USER, CREATE_USER } = require('./app/common')

describe('AUTH CONTROLLER', () => {
  let refreshToken = ''
  let accessToken = ''

  before(async () => {
    const token = await CREATE_USER()
    accessToken = token.access_token
    refreshToken = token.refresh_token
  })

  describe('[POST] /auth/login', () => {
    it('it should return auth/login tokens', done => {
      chai.request(URL)
        .post('/auth/login')
        .set('content-type', 'application/json')
        .send({ password: USER.password, email: USER.email })
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.status).to.equal(200)
          expect(res.body).to.be.a('object')
          expect(res.body.access_token).to.be.a('string').that.is.not.empty
          expect(res.body.refresh_token).to.be.a('string').that.is.not.empty

          refreshToken = res.body.refresh_token
          accessToken = res.body.access_token
          done()
        })
    })

    it('it should return validation error', done => {
      chai.request(URL)
        .post('/auth/login')
        .set('content-type', 'application/json')
        .send({ password: USER.password })
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.status).to.equal(422)
          expect(res.body.message).to.equal('Validation Failed')
          expect(res.body.errors).to.deep.include({ email: 'Please enter your email' })

          done()
        })
    })
  })

  describe('[POST] /auth/refresh-tokens', () => {
    it('it should return refreshed access/refresh tokens', done => {
      chai.request(URL)
        .post('/auth/refresh-tokens')
        .set('content-type', 'application/json')
        .send({ refresh_token: refreshToken })
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.status).to.equal(200)
          expect(res.body).to.be.a('object')
          expect(res.body.access_token).to.be.a('string').that.is.not.empty
          expect(res.body.refresh_token).to.be.a('string').that.is.not.empty

          refreshToken = res.body.refresh_token
          accessToken = res.body.access_token
          done()
        })
    })

    it('it should return bad request', done => {
      chai.request(URL)
        .post('/auth/refresh-tokens')
        .set('content-type', 'application/json')
        .send({ refresh_token: '1234' })
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.status).to.equal(403)
          expect(res.body).to.be.a('object')
          expect(res.body.status).to.equal(ResponseCode.INVALID_CREDENTIALS.status)
          expect(res.body.code).to.equal(ResponseCode.INVALID_CREDENTIALS.code)
          expect(res.body.message).to.equal(ResponseCode.INVALID_CREDENTIALS.message)

          done()
        })
    })
  })

  describe('[POST] /auth/logout', () => {
    it('it should return success message', done => {
      chai.request(URL)
        .post('/auth/logout')
        .set('content-type', 'application/json')
        .set('authorization', `Bearer ${accessToken}`)
        .send()
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.status).to.equal(204)

          done()
        })
    })

    it('it should return token verify error', done => {
      chai.request(URL)
        .post('/auth/logout')
        .set('content-type', 'application/json')
        .set('authorization', 'Bearer 1234567')
        .end((err, res) => {
          expect(err).to.be.null
          expect(res.status).to.equal(401)
          expect(res.body).to.be.a('object')
          expect(res.body.status).to.equal(ResponseCode.TOKEN_VERIFY.status)
          expect(res.body.code).to.equal(ResponseCode.TOKEN_VERIFY.code)
          expect(res.body.message).to.equal(ResponseCode.TOKEN_VERIFY.message)

          done()
        })
    })
  })
})
