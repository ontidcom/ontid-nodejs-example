const { KeyCloak } = require('../../app/core/lib')
const { UserModel } = require('../../app/models')
const { host, port } = require('../../app/config').app

const URL = `${host}:${port}`

const USER = {
  first_name: 'First name',
  last_name: 'Last name',
  email: 'test@test.com',
  password: '12345678',
  phone: '+375111111111'
}

/**
 * Create user
 *
 * @returns {Promise<*>}
 */
const CREATE_USER = async () => {
  const userModel = await UserModel.findByEmailOrCreate({ email: USER.email, phone: USER.phone })
  const data = await KeyCloak.User.findEmailGetId(USER.email)

  if (data === null) {
    await KeyCloak.User.create({ ...USER, id: userModel.id })
  }
  await userModel.$relatedQuery('profile').insert({ first_name: USER.first_name, last_name: USER.last_name })
  return KeyCloak.Auth.login(USER)
}

module.exports = { URL, USER, CREATE_USER }
