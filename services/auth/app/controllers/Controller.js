const logger = require('../core/Logger')

class Controller {
  async init () {
    logger.trace(`${this.constructor.name} initialized...`)
  }
}

module.exports = { Controller }
