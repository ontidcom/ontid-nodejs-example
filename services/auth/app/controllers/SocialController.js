const { UserModel } = require('../models')
const { FacebookClient, LinkedinClient } = require('../core/client')
const { Controller } = require('./Controller')
const { KeyCloak } = require('../core/lib')
const { TokenResource } = require('../core/http/resources')

class SocialController extends Controller {
  /**
   * Facebook
   *
   * @param req
   * @param res
   */
  static async facebook (req, res) {
    const data = await FacebookClient.getData(req.body.access_token)
    const token = await SocialController.createOrAuthSocial(data, 'facebook')
    const fullRegistration = await UserModel.checkAddressByToken(token)

    UserModel.stats(token, 'facebook').then()

    res.json(TokenResource.make({ ...token, fullRegistration }))
  }

  /**
   * Linkedin
   *
   * @param req
   * @param res
   */
  static async linkedin (req, res) {
    const data = await LinkedinClient.getData(req.body.access_token)
    const token = await SocialController.createOrAuthSocial(data, 'linkedin')
    const fullRegistration = await UserModel.checkAddressByToken(token)

    UserModel.stats(token, 'linkedin').then()

    res.json(TokenResource.make({ ...token, fullRegistration }))
  }

  /**
   * Create or auth social
   *
   * TODO: optimization
   *
   * @param data
   * @param provider
   * @returns Promise<*>
   */
  static async createOrAuthSocial (data, provider) {
    const user = await UserModel.findByEmailOrCreate({ email: data.email })
    let token = await KeyCloak.Auth.tokenExchange(user.email)

    if (!token) {
      await KeyCloak.User.create({ ...user, first_name: data.first_name, last_name: data.last_name })
      await UserModel.insertProfile(user.id, {
        first_name: data.first_name,
        last_name: data.last_name,
        avatar: data.avatar
      })
      const userKeyCloakId = await KeyCloak.User.findEmailGetId(user.email)
      await KeyCloak.Provider.create(userKeyCloakId || '', provider, data)
      token = await KeyCloak.Auth.tokenExchange(user.email)
    }

    return token
  }
}

module.exports = { SocialController }
