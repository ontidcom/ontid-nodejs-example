const { AuthController } = require('./AuthController')
const { SocialController } = require('./SocialController')

module.exports = {
  AuthController,
  SocialController
}
