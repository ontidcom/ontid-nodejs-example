const { KeyCloak } = require('../core/lib')
const { Controller } = require('./Controller')
const { TokenResource } = require('../core/http/resources')
const { UserModel } = require('../models')

class AuthController extends Controller {
  /**
   * Login user
   *
   * @param req
   * @param res
   */
  static async login (req, res) {
    const { email, password } = req.body
    const token = await KeyCloak.Auth.login({ email, password })
    const fullRegistration = await UserModel.checkAddressByToken(token)

    UserModel.stats(token, 'auth').then()

    res.json(TokenResource.make({ ...token, fullRegistration }))
  }

  /**
   * Refresh token
   *
   * @param req
   * @param res
   */
  static async refreshToken (req, res) {
    const { refresh_token: refreshToken } = req.body
    const token = await KeyCloak.Auth.refreshToken(refreshToken)
    const fullRegistration = await UserModel.checkAddressByToken(token)

    UserModel.stats(token, 'auth').then()

    res.json(TokenResource.make({ ...token, fullRegistration }))
  }

  /**
   * Logout
   *
   * @param req
   * @param res
   */
  static async logout (req, res) {
    const keycloakUserId = req.currentUser.id
    await KeyCloak.Auth.logout(keycloakUserId)

    res.status(204).json()
  }
}

module.exports = { AuthController }
