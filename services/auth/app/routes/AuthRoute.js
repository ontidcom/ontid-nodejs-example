const express = require('express')
const CheckAccessTokenMiddleware = require('../core/middleware/CheckAccessTokenMiddleware')
const AsyncMiddleware = require('../core/middleware/AsyncMiddleware')
const { AuthRule, validate } = require('../core/validator')
const { AuthController, SocialController } = require('../controllers')

const router = express.Router()

router.post('/login', AuthRule.login(), validate, AsyncMiddleware(AuthController.login))
router.post('/facebook', AuthRule.token(), validate, AsyncMiddleware(SocialController.facebook))
router.post('/linkedin', AuthRule.token(), validate, AsyncMiddleware(SocialController.linkedin))
router.post('/refresh-tokens', AuthRule.refreshToken(), validate, AsyncMiddleware(AuthController.refreshToken))
router.post('/logout', CheckAccessTokenMiddleware.handler(), AsyncMiddleware(AuthController.logout))

module.exports = router
