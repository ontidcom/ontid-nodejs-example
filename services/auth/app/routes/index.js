const AuthRoute = require('./AuthRoute')

module.exports = app => {
  app.use('/auth', AuthRoute)

  app.use((req, res, next) => !req.route ? next(new Error('ROUTE_NOT_FOUND')) : next())
}
