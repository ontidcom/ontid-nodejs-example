const express = require('express')
const promBundle = require('express-prom-bundle')
const metricsMiddleware = promBundle({ includeMethod: true })
const bodyParser = require('body-parser')
const { Assert: assert } = require('./core/lib/assert')
const { BaseMiddleware, Logger } = require('./core/lib')
const routes = require('./routes')

class Server {
  constructor ({ port, middleware, errorMiddleware, logger }) {
    assert.integer(port, { required: true, positive: true })
    assert.array(middleware, { required: true, notEmpty: true, message: 'middleware param expects not empty array' })
    assert.instanceOf(errorMiddleware, BaseMiddleware)
    assert.instanceOf(logger, Logger)

    logger.info('Server start initialization...')
    return start({ port, middleware, errorMiddleware, logger })
  }
}

/**
 * Start server
 *
 * @param port
 * @param host
 * @param middleware
 * @param errorMiddleware
 * @param logger
 * @returns {Promise<unknown>}
 */
function start ({ port, middleware, errorMiddleware, logger }) {
  return new Promise(async (resolve, reject) => {
    const app = express()

    app.use(metricsMiddleware)
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: false }))

    /**
     * middleware initialization
     */
    for (const m of middleware) {
      try {
        await m.init()
        app.use(m.handler())
      } catch (e) {
        return reject(e)
      }
    }

    routes(app)

    /**
     * error handler
     */
    try {
      await errorMiddleware.init()
      app.use(errorMiddleware.handler())
    } catch (e) {
      return reject(`Default error middleware failed. ${e}`)
    }

    process.on('unhandledRejection', (reason, promise) => {
      logger.error('unhandledRejection', reason)
    })

    process.on('rejectionHandled', promise => {
      logger.warn('rejectionHandled', promise)
    })

    process.on('multipleResolves', (type, promise, reason) => {
      logger.error('multipleResolves', { type, promise, reason })
    })

    process.on('uncaughtException', error => {
      logger.fatal('uncaughtException', error.stack)
      process.exit(1)
    })

    return app.listen(port, () => resolve({ port }))
  })
}

module.exports = { Server }
