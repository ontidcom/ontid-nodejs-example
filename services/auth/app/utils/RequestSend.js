const request = require('request')
const logger = require('../core/Logger')

class RequestSend {
  /**
   * Send request
   *
   * @returns {Promise<*>}
   */
  static async send (req) {
    return new Promise((resolve, reject) => {
      request(req, (error, response, body) => {
        if (error) return reject(error)
        try {
          const res = typeof body === 'undefined' ? {} : (typeof body === 'string' ? JSON.parse(body) : body)

          res.statusCode = response.statusCode || ''
          res.statusMessage = res.errorMessage || response.statusMessage || ''
          res.error = res.errorMessage || res.error || ''
          if (res.error) {
            logger.error('RequestSend', res)
            reject(res)
          }
          resolve(res)
        } catch (e) {
          logger.error('RequestSend', error)
          reject(e)
        }
      })
    })
  }
}

module.exports = { RequestSend }
