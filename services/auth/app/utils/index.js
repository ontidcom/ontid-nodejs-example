const { JWT } = require('./JWT')
const { RequestSend } = require('./RequestSend')

module.exports = {
  JWT,
  RequestSend
}
