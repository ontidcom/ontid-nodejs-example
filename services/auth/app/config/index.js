const app = require('./app')
const keyCloak = require('./keycloak')
const knex = require('./knex')
const facebook = require('./facebook')
const linkedin = require('./linkedin')

module.exports = {
  app,
  keyCloak,
  knex,
  facebook,
  linkedin
}
