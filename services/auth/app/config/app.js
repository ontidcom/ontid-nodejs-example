const { BaseConfig } = require('../core/lib/BaseConfig')

class App extends BaseConfig {
  constructor () {
    super()
    this.nodeEnv = this.set('NODE_ENV', v => ['development', 'production', 'test'].includes(v), 'development')
    this.port = this.set('APP_PORT', this.joi.number().port().required(), 5555)
    this.host = this.set('APP_HOST', this.joi.string().required(), 'http://localhost')
    this.name = this.set('APP_NAME', this.joi.string().required(), 'auth')
  }
}

module.exports = new App()
