const { BaseConfig } = require('../core/lib/BaseConfig')

class Facebook extends BaseConfig {
  constructor () {
    super()
    this.url = this.set('FACEBOOK_URL', this.joi.string().required(), '')
    this.fields = this.set('FACEBOOK_FIELDS', this.joi.string().required(), '')
  }
}

module.exports = new Facebook()
