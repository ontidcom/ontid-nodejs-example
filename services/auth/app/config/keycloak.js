const { BaseConfig } = require('../core/lib/BaseConfig')

class Keycloak extends BaseConfig {
  constructor () {
    super()
    this.authServerUrl = this.set('AUTH_SERVER_URL', this.joi.string(), '-')
    this.adminUsername = this.set('ADMIN_USERNAME', this.joi.string(), '-')
    this.adminPassword = this.set('ADMIN_PASSWORD', this.joi.string(), '-')
    this.clientId = this.set('CLIENT_ID', this.joi.string(), '-')
    this.grantType = this.set('GRANT_TYPE', this.joi.string(), '-')
    this.realmName = this.set('REALM_NAME', this.joi.string(), '-')
    this.clientSecret = this.set('CREDENTIAL_SECRET', this.joi.string(), '-')
  }
}

module.exports = new Keycloak()
