const { BaseConfig } = require('../core/lib/BaseConfig')

class Linkedin extends BaseConfig {
  constructor () {
    super()
    this.email = this.set('LINKEDIN_EMAIL', this.joi.string().required(), '')
    this.profile = this.set('LINKEDIN_PROFILE', this.joi.string().required(), '')
  }
}

module.exports = new Linkedin()
