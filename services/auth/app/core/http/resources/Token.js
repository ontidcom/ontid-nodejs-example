class Token {
  /**
   * Make response
   *
   * @param data
   * @returns object
   */
  static make (data) {
    return {
      full_registration: data.fullRegistration || false,
      access_token: data.access_token,
      refresh_token: data.refresh_token,
      expires_in: data.expires_in
    }
  }
}

module.exports = { Token }
