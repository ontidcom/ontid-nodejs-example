const config = require('../../config')
const { RequestSend } = require('../../utils')
const { BaseClient, ResponseCode } = require('../lib')
const logger = require('../Logger')

class LinkedinClient extends BaseClient {
  /**
   * Get profile
   *
   * @param accessToken
   * @returns {Promise<*>}
   */
  static async getProfile (accessToken) {
    const req = {
      method: 'GET',
      auth: {
        bearer: accessToken
      },
      url: config.linkedin.profile
    }

    return RequestSend.send(req).then(res => {
      const id = res && res.id ? res.id : ''
      const firstName = res && res['localizedFirstName'] || ''
      const lastName = res && res['localizedLastName'] || ''
      const avatar = res && res['profilePicture'] && res['profilePicture']['displayImage~'] && res['profilePicture']['displayImage~']['elements'] && res['profilePicture']['displayImage~']['elements'][1] && res['profilePicture']['displayImage~']['elements'][1]['identifiers'] && res['profilePicture']['displayImage~']['elements'][1]['identifiers'][0] && res['profilePicture']['displayImage~']['elements'][1]['identifiers'][0]['identifier'] || ''

      return { id, first_name: firstName, last_name: lastName, avatar }
    }).catch((e) => {
      logger.error('LinkedinClient - getProfile', e)
      this.responseError(ResponseCode.TOKEN_VERIFY, 'Linkedin')
    })
  }

  /**
   * Get email
   *
   * @param accessToken
   * @returns {Promise<*>}
   */
  static async getEmail (accessToken) {
    const req = {
      method: 'GET',
      auth: {
        bearer: accessToken
      },
      url: config.linkedin.email
    }

    return RequestSend.send(req).then(res => res && res.elements && res.elements[0] ? res.elements[0]['handle~']['emailAddress'].toLowerCase() : this.responseError(ResponseCode.EMPTY_EMAIL, 'Linkedin')).catch((e) => {
      logger.error('LinkedinClient - getEmail', e)
      return e.code === 'EMPTY_EMAIL_ERROR' ? this.responseError(e, 'Linkedin') : this.responseError(ResponseCode.TOKEN_VERIFY, 'Linkedin')
    })
  }

  /**
   * Get data
   *
   * @param accessToken
   * @returns {Promise<*>}
   */
  static async getData (accessToken) {
    const email = await LinkedinClient.getEmail(accessToken)
    const profile = await LinkedinClient.getProfile(accessToken)

    return { email, ...profile }
  }
}

module.exports = { LinkedinClient }
