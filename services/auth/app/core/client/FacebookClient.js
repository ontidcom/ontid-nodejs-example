const config = require('../../config')
const { RequestSend } = require('../../utils')
const { BaseClient, ResponseCode } = require('../lib')
const logger = require('../Logger')

const url = `${config.facebook.url}?fields=${config.facebook.fields}&access_token=`

class FacebookClient extends BaseClient {
  /**
   * Get data
   *
   * @param accessToken
   * @returns {Promise<*>}
   */
  static async getData (accessToken) {
    const req = {
      method: 'GET',
      url: url + accessToken
    }
    return RequestSend.send(req).then(res => {
      if (res.email) {
        res.email = res.email.toLowerCase()
        res.avatar = res.picture && res.picture.data && res.picture.data.url || ''
        return res
      }
      return this.responseError(ResponseCode.EMPTY_EMAIL, 'Facebook')
    }).catch((e) => {
      logger.error('FacebookClient', e)
      return e.code === 'EMPTY_EMAIL_ERROR' ? this.responseError(e, 'Facebook') : this.responseError(ResponseCode.TOKEN_VERIFY, 'Facebook')
    })
  }
}

module.exports = { FacebookClient }
