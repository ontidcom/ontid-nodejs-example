const { FacebookClient } = require('./FacebookClient')
const { LinkedinClient } = require('./LinkedinClient')

module.exports = {
  FacebookClient,
  LinkedinClient
}
