const { AppError } = require('./AppError')

class BaseClient {
  /**
   * Response error
   *
   * @param error
   * @param layer
   */
  static responseError (error, layer) {
    throw new AppError({ ...error, layer: layer })
  }
}

module.exports = { BaseClient }
