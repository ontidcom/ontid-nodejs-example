const config = require('../../../config')
const { BaseKeyCloak } = require('../BaseKeycloak')

class Provider extends BaseKeyCloak {
  /**
   * Create provider
   *
   * @param {string} id
   * @param provider
   * @param {object} user - The JSON representation of a user
   * @returns {Promise} A promise that will resolve with the user object
   */
  static async create (id, provider, user) {
    const token = await this.getAccessToken()

    const req = {
      url: `${config.keyCloak.authServerUrl}/admin/realms/${config.keyCloak.realmName}/users/${id}/federated-identity/${provider}`,
      method: 'POST',
      auth: {
        bearer: token
      },
      body: {
        identityProvider: provider,
        userId: user.id || '',
        userName: user.email || ''
      },
      json: true
    }

    return this.request(req).catch(e => this.errorResponse(e))
  }
}

module.exports = { Provider }
