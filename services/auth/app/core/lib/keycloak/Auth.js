const request = require('request')
const config = require('../../../config')
const { BaseKeyCloak } = require('../BaseKeycloak')

class Auth extends BaseKeyCloak {
  /**
   * A function to get the login from KeyCloak
   *
   * @param {object} [options] - The options object
   * @param {string} [options.email] - The username (email)
   * @param {string} [options.password] - The password
   * @returns {Promise}
   */
  static async login (options) {
    return this.getToken({}, { username: options.email, password: options.password })
  }

  /**
   * Logout keycloak
   *
   * @param userId
   * @returns {Promise<*>}
   */
  static async logout (userId) {
    const token = await this.getAccessToken()
    const req = {
      url: `${config.keyCloak.authServerUrl}/admin/realms/${config.keyCloak.realmName}/users/${userId}/logout`,
      method: 'POST',
      auth: {
        bearer: token
      },
      json: true
    }

    return this.request(req).catch(e => this.errorResponse(e))
  }

  /**
   * Token exchange
   *
   * @param email
   * @returns {Promise<*>}
   */
  static async tokenExchange (email) {
    const req = {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      form: {
        client_id: config.keyCloak.clientId,
        client_secret: config.keyCloak.clientSecret,
        grant_type: 'urn:ietf:params:oauth:grant-type:token-exchange',
        requested_subject: email
      },
      url: `${config.keyCloak.authServerUrl}/realms/${config.keyCloak.realmName}/protocol/openid-connect/token`,
      json: true
    }

    return new Promise((resolve, reject) => {
      request(req, (error, response, body) => {
        if (error) return resolve(null)
        try {
          const res = typeof body === 'undefined' ? {} : (typeof body === 'string' ? JSON.parse(body) : body)
          if (res.error) {
            resolve(null)
          }
          resolve(res)
        } catch (e) {
          resolve(null)
        }
      })
    })
  }

  /**
   * A function to get the refresh token from KeyCloak
   *
   * @param {string} [refreshToken] - The refresh_token
   * @returns {Promise}
   */
  static async refreshToken (refreshToken) {
    const req = {
      form: {
        grant_type: 'refresh_token',
        refresh_token: refreshToken
      }
    }

    return this.getToken(req)
  }
}

module.exports = { Auth }
