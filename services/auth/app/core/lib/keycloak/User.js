const request = require('request')
const config = require('../../../config')
const { BaseKeyCloak } = require('../BaseKeycloak')
const logger = require('../../Logger')

class User extends BaseKeyCloak {
  /**
   * A function to create a new user for a realm.
   *
   * @param {object} user - The JSON representation of a user
   * @returns {Promise} A promise that will resolve with the user object
   */
  static async create (user) {
    const token = await this.getAccessToken()

    const req = {
      url: `${config.keyCloak.authServerUrl}/admin/realms/${config.keyCloak.realmName}/users`,
      method: 'POST',
      auth: {
        bearer: token
      },
      body: {
        username: user.email,
        email: user.email,
        firstName: user.first_name || '',
        lastName: user.last_name || '',
        enabled: true,
        attributes: { userId: user.id, phone: user.phone || '' }
      },
      json: true
    }

    if (user.password) {
      req.body.credentials = [{
        type: 'password',
        value: user.password,
        temporary: false
      }]
    }

    return this.request(req).catch(e => e.statusCode === 409 ? this.errorEmailAlreadyTaken() : this.errorResponse(e))
  }

  /**
   * Find by email get id
   *
   * @param email
   * @returns {Promise<*>}
   */
  static async findEmailGetId (email) {
    const token = await this.getAccessToken()

    const req = {
      url: `${config.keyCloak.authServerUrl}/admin/realms/${config.keyCloak.realmName}/users?email=${email}&max=1`,
      method: 'GET',
      auth: {
        bearer: token
      }
    }

    return new Promise(resolve => {
      request(req, (error, response, body) => {
        if (error) return resolve(null)
        try {
          const res = typeof body === 'undefined' ? {} : (typeof body === 'string' ? JSON.parse(body) : body)

          resolve(res[0] && res[0].id ? res[0].id : null)
        } catch (e) {
          logger.error('KeyCloak - Auth', e)
          resolve(null)
        }
      })
    })
  }
}

module.exports = { User }
