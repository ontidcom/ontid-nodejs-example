const { Model } = require('objection')
const { AppError } = require('./AppError')
const ResponseCode = require('./ResponseCode')
const logger = require('../Logger')

class BaseModel extends Model {
  /**
   * Error not found
   */
  static errorNotFound () {
    throw new AppError({ ...ResponseCode.NOT_FOUND, layer: 'Model' })
  }

  /**
   * Error DB
   */
  static errorDB () {
    throw new AppError({ ...ResponseCode.DB, layer: 'Model' })
  }

  /**
   * create
   *
   * @param params
   * @returns {Promise<*>}
   */
  static async create (params) {
    const data = await this.query().insert(params).returning('*').catch(e => {
      logger.error('BaseModel - create', e)
      return this.errorDB()
    })

    return !data ? this.errorNotFound() : data
  }

  /**
   * Find or create
   *
   * @param params
   * @param data
   * @returns {Promise<*>}
   */
  static async findOrCreate (params, data) {
    const query = await this.query().findOne(params).catch(e => {
      logger.error('BaseModel - findOrCreate', e)
      return this.errorDB()
    })

    return !query ? this.create(data) : query
  }

  /**
   * Update at
   */
  $beforeUpdate () {
    if (this.updated_at) {
      this.updated_at = new Date().toISOString()
    }
  }
}

module.exports = { BaseModel }
