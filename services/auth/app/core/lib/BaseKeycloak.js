const config = require('../../config')
const { RequestSend } = require('../../utils')
const { AppError } = require('./AppError')
const ResponseCode = require('./ResponseCode')

class BaseKeycloak {
  /**
   * Error response
   *
   * @param err
   */
  static errorResponse (err) {
    if (err && err.error && err.error === 'invalid_grant') {
      throw new AppError(ResponseCode.INVALID_CREDENTIALS)
    } else {
      throw new AppError({ status: err.statusCode, message: err.statusMessage, layer: 'KeyCloak' })
    }
  }

  static errorEmailAlreadyTaken () {
    throw new AppError(ResponseCode.EMAIL_ALREADY_TAKEN)
  }

  /**
   * A function to get the admin getToken from KeyCloak
   *
   * @returns {Promise}
   */
  static async getToken (req = {}, options = {}) {
    if (typeof req.form === 'undefined') {
      req = {
        form: {
          grant_type: config.keyCloak.grantType,
          username: options.username || config.keyCloak.adminUsername,
          password: options.password || config.keyCloak.adminPassword
        }
      }
    }

    req.url = `${config.keyCloak.authServerUrl}/realms/${config.keyCloak.realmName}/protocol/openid-connect/token`
    req.method = 'POST'
    req.headers = { 'Content-Type': 'application/x-www-form-urlencoded' }
    req.form.client_id = config.keyCloak.clientId
    req.form.client_secret = config.keyCloak.clientSecret

    return this.request(req).catch(e => this.errorResponse(e))
  }

  /**
   * Get only access token
   *
   * @returns {Promise<*|string>}
   */
  static async getAccessToken () {
    const token = await BaseKeycloak.getToken()
    return token['access_token'] || ''
  }

  /**
   * Send request to keyCloak
   *
   * @param req
   * @returns {Promise<*>}
   */
  static async request (req) {
    return RequestSend.send(req)
  }
}

module.exports = { BaseKeyCloak: BaseKeycloak }
