const { body } = require('express-validator')
const lang = require('../../locales/en')

class AuthRule {
  /**
   * Login
   */
  static login () {
    return [
      body('email', lang.validator.email).isEmail().normalizeEmail(),
      body('password', lang.validator['password.length']).isLength({ min: 6 })
    ]
  }

  /**
   * Token
   */
  static token () {
    return [
      body('access_token', lang.validator.access_token).notEmpty().trim()
    ]
  }

  /**
   * Refresh token
   */
  static refreshToken () {
    return [
      body('refresh_token', lang.validator.refresh_token).notEmpty().trim()
    ]
  }
}

module.exports = { AuthRule }
