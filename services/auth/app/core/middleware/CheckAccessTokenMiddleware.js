const { ResponseCode, BaseMiddleware, AppError } = require('../lib')
const { JWT } = require('../../utils')
const logger = require('../Logger')

class CheckAccessTokenMiddleware extends BaseMiddleware {
  /**
   * Init check access token middleware
   */
  async init () {
    logger.trace(`${this.constructor.name} initialized...`)
  }

  /**
   * Token error
   * @returns {*}
   */
  get tokenNotSigned () {
    return new AppError({ ...ResponseCode.TOKEN_NOT_SIGNED, layer: 'Token' })
  }

  /**
   * Handler
   *
   * @returns {Function}
   */
  handler () {
    return (req, res, next) => {
      const authorization = req.headers['authorization'] || req.headers['Authorization']
      const bearer = authorization && authorization.startsWith('Bearer ') ? authorization : null
      const token = bearer ? bearer.split('Bearer ')[1] : null

      req.currentUser = Object.freeze({
        id: null,
        user_id: null,
        email: null,
        expiresIn: null,
        role: []
      })

      if (token) {
        return JWT.verify(token)
          .then(data => {
            req.currentUser = Object.freeze({
              id: data.sub,
              user_id: data.user_id,
              email: data.email,
              expiresIn: Number(data.exp),
              role: data.realm_access && data.realm_access.roles || []
            })
            next()
          }).catch(e => {
            logger.error('CheckAccessTokenMiddleware', e)
            next(e)
          })
      } else {
        next(this.tokenNotSigned)
      }
    }
  }
}

module.exports = new CheckAccessTokenMiddleware()
