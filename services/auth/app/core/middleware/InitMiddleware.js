const { BaseMiddleware } = require('../lib')
const config = require('../../config')
const logger = require('../Logger')

class InitMiddleware extends BaseMiddleware {
  /**
   * Init controller
   */
  async init () {
    logger.trace(`${this.constructor.name} initialized...`)
  }

  /**
   * Handler
   *
   * @returns {Function}
   */
  handler () {
    return (req, res, next) => {
      res.header('Server', config.app.name)
      next()
    }
  }
}

module.exports = new InitMiddleware()
