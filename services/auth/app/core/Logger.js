const config = require('../config')
const { Logger } = require('./lib/Logger')

module.exports = new Logger({
  appName: config.app.name
})
