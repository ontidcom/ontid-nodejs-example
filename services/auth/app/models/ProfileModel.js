const { BaseModel } = require('../core/lib')

class ProfileModel extends BaseModel {
  static get tableName () {
    return 'profiles'
  }

  static get jsonSchema () {
    return {
      type: 'object',
      properties: {
        avatar: { type: ['string', 'null'] },
        first_name: { type: ['string', 'null'] },
        lats_name: { type: ['string', 'null'] }
      }
    }
  }
}

module.exports = ProfileModel
