const moment = require('moment')
const { Model } = require('objection')
const { BaseModel } = require('../core/lib')
const logger = require('../core/Logger')
const { JWT } = require('../utils')

class UserModel extends BaseModel {
  static get tableName () {
    return 'users'
  }

  static get jsonSchema () {
    return {
      type: 'object',
      properties: {
        phone: { type: 'string' }, 
        email: { type: 'string' },
        confirm_token_email: { type: ['string', 'null'] },
        confirm_token_phone: { type: ['string', 'null'] },
        confirm_token_reset: { type: ['string', 'null'] },
        is_confirmed_email: { type: 'boolean' },
        is_confirmed_phone: { type: 'boolean' }
      }
    }
  }

  static get relationMappings () {
    const ProfileModel = require('./ProfileModel')

    return {
      profile: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProfileModel,
        join: {
          from: 'users.profile_id',
          to: 'profiles.id'
        }
      }
    }
  }

  /**
   * Find by email or create user
   *
   * @param params
   * @returns {Promise<*>}
   */
  static async findByEmailOrCreate (params) {
    return this.findOrCreate({ email: params.email }, { ...params, is_confirmed_email: true })
  }

  /**
   * Insert profile
   *
   * @param userID
   * @param data
   * @returns {Promise<*>}
   */
  static async insertProfile (userID, data) {
    return this.relatedQuery('profile')
      .for(userID)
      .insert(data)
      .catch(e => {
        logger.error('UserModel - updateProfile', e)
        return this.errorDB()
      })
  }
}

module.exports = UserModel
