const UserModel = require('./UserModel')
const ProfileModel = require('./ProfileModel')

module.exports = {
  UserModel,
  ProfileModel
}
