const app = require('./app')
const knex = require('./knex')
const role = require('./role')

module.exports = {
  app,
  knex,
  role
}
