const { BaseConfig } = require('../core/lib/BaseConfig')

class Knex extends BaseConfig {
  constructor () {
    super()
    this.client = 'pg'
    this.connection = {
      host: this.set('DB_HOST', this.joi.string().min(4).max(100).required(), 'localhost'),
      port: this.set('DB_PORT', this.joi.number().required(), '5432'),
      user: this.set('DB_USER', this.joi.string().min(4).max(100).required(), 'postgres'),
      password: this.set('DB_PASSWORD', this.joi.string().required()),
      database: this.set('DB_NAME', this.joi.string().min(4).max(100).required()),
      charset: this.set('DB_CHARSET', this.joi.string().valid('utf8').required(), 'utf8')
    }

    const debug = this.set('DB_DEBUG', this.joi.boolean(), false)
    this.debug = (debug === 'true')
  }
}

module.exports = new Knex()
