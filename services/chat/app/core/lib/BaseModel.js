const { Model } = require('objection')
const { AppError } = require('./AppError')
const ResponseCode = require('./ResponseCode')
const logger = require('../Logger')

class BaseModel extends Model {
  /**
   * Response Code
   *
   * @returns {module.exports.SENT_SMS|{code, message, status}}
   */
  static responseCode (code) {
    return ResponseCode[code]
  }

  /**
   * Error not found
   */
  static errorNotFound () {
    throw new AppError({ ...ResponseCode.NOT_FOUND, layer: 'Model' })
  }

  /**
   * Error unprocessable entity
   */
  static errorUnprocessableEntity () {
    throw new AppError({ ...ResponseCode.UNPROCESSABLE_ENTITY, layer: 'Model' })
  }

  /**
   * Error DB
   */
  static errorDB () {
    throw new AppError({ ...ResponseCode.DB, layer: 'Model' })
  }

  /**
   * response error
   */
  static responseError (code) {
    throw new AppError({ ...BaseModel.responseCode(code), layer: 'Model' })
  }

  /**
   * Empty list response
   *
   * @returns {Array}
   */
  static emptyListResponse () {
    return []
  }

  /**
   * Empty object response
   *
   * @returns {{}}
   */
  static emptyObjectResponse () {
    return {}
  }

  /**
   * Get one by where
   *
   * @param params
   * @returns {Promise<*>}
   */
  static async getOneByWhere (params) {
    const data = await this.query().where(params).first().catch(e => {
      logger.error('BaseModel - getOneByWhere', e)
      return this.errorDB()
    })

    return !data ? this.errorNotFound() : data
  }

  /**
   * create
   *
   * @param params
   * @returns {Promise<*>}
   */
  static async create (params) {
    const data = await this.query().insert(params).returning('*').catch(e => {
      logger.error('BaseModel - create', e)
      return this.errorDB()
    })

    return !data ? this.errorNotFound() : data
  }

  /**
   * Find or create
   *
   * @param params
   * @param data
   * @returns {Promise<*>}
   */
  static async findOrCreate (params, data) {
    const query = await this.query().where(params).first().catch(e => {
      logger.error('BaseModel - findOrCreate', e)
      return this.errorDB()
    })

    return !query ? this.create(data) : query
  }

  /**
   * Update or create
   *
   * @param params
   * @param data
   * @returns {Promise<*>}
   */
  static async updateOrCreate (params, data) {
    const query = await this.query().where(params).first().catch(e => {
      logger.error('BaseModel - findOrCreate', e)
      return this.errorDB()
    })

    return !query ? this.create(data) : this.update(params, data)
  }

  /**
   * Get one by where
   *
   * @param params
   * @param data
   * @returns {Promise<*>}
   */
  static async update (params, data) {
    const query = await this.query().where(params).update(data).catch(e => {
      logger.error('BaseModel - update', e)
      return this.errorDB()
    })

    return !query ? this.errorNotFound() : query
  }

  /**
   * Remove by id
   *
   * @param id
   * @returns {Promise<*>}
   */
  static async removeById (id) {
    return this.query().deleteById(id).catch(e => {
      logger.error('BaseModel - update', e)
      return this.errorDB()
    })
  }

  /**
   * Update at
   */
  $beforeUpdate () {
    if (this.updated_at) {
      this.updated_at = new Date().toISOString()
    }
  }
}

module.exports = { BaseModel }
