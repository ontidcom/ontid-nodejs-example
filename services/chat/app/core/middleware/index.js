const InitsMiddleware = require('./InitMiddleware')
const CorsMiddleware = require('./CorsMiddleware')

module.exports = [
  InitsMiddleware,
  CorsMiddleware
]
