const { ResponseCode, BaseMiddleware, AppError } = require('../lib')
const { RoleAccess } = require('../../utils')
const logger = require('../Logger')

class PermissionMiddleware extends BaseMiddleware {
  /**
   * Init check access token middleware
   */
  async init () {
    logger.trace(`${this.constructor.name} initialized...`)
  }

  /**
   * Forbidden error
   * @returns {*}
   */
  get forbidden () {
    return new AppError({ ...ResponseCode.FORBIDDEN, layer: 'Permission' })
  }

  /**
   * Handler
   *
   * @returns {Function}
   */
  handler (role) {
    return (req, res, next) => {
      if (RoleAccess.access(req.currentUser, role)) {
        return next()
      }

      return next(this.forbidden)
    }
  }
}

module.exports = new PermissionMiddleware()
