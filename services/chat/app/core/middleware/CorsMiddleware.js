const { BaseMiddleware } = require('../lib')
const logger = require('../Logger')

class CorsMiddleware extends BaseMiddleware {
  /**
   * Init cors middleware
   */
  async init () {
    logger.trace(`${this.constructor.name} initialized...`)
  }

  /**
   * Handler
   *
   * @returns {Function}
   */
  handler () {
    return (req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*')
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, authorization, Authorization')
      res.header('Access-Control-Expose-Headers', 'X-Total-Count')
      next()
    }
  }
}

module.exports = new CorsMiddleware()
