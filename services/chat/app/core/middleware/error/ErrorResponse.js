const uuid4 = require('uuid/v4')

class ErrorResponse {
  constructor (options = {}) {
    this.logId = uuid4()
    this.status = options.status || undefined
    this.code = options.code || undefined
    this.message = options.message || undefined
    this.src = options.src || undefined
  }
}

module.exports = ErrorResponse
