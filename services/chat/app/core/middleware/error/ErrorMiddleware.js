const stackTrace = require('stack-trace')
const ErrorResponse = require('./ErrorResponse')
const { ResponseCode, BaseMiddleware } = require('../../lib')
const logger = require('../../Logger')

class ErrorMiddleware extends BaseMiddleware {
  /**
   * Init error middleware
   *
   * @returns {Promise<void>}
   */
  async init () {
    logger.trace(`${this.constructor.name} initialized...`)
  }

  /**
   * Handler
   *
   * @returns {Function}
   */
  handler () {
    return (error, req, res, next) => {
      if (error.status === 404) {
        const errorRes = new ErrorResponse({
          ...ResponseCode.NOT_FOUND
        })

        logger.error('ErrorMiddleware', error)
        res.status(errorRes.status).json(errorRes)
      } else if (error.message === 'ROUTE_NOT_FOUND') {
        const errorRes = new ErrorResponse({
          ...ResponseCode.ROUTE_NOT_FOUND
        })

        logger.error('ErrorMiddleware', error)
        res.status(errorRes.status).json(errorRes)
      } else {
        const errorRes = new ErrorResponse({
          ...error,
          message: error.message || error,
          code: error.code || ResponseCode.SERVER.code,
          status: error.status || ResponseCode.SERVER.status
        })

        errorRes.stack = stackTrace.parse(error)
        logger.error(errorRes.message, error, { ...errorRes, req: error.req, meta: error.meta })
        delete errorRes.stack

        res.status(errorRes.status).json(errorRes)
      }
    }
  }
}

module.exports = new ErrorMiddleware()
