const ErrorResponse = require('../middleware/error/ErrorResponse')
const { ResponseCode } = require('../lib')
const { JWT } = require('../../utils')

class Authorization {
  /**
   * handler
   *
   * @param client
   * @param next
   */
  static handler (client, next) {
    if (client.handshake.query && client.handshake.query.token) {
      JWT.verify(client.handshake.query.token).then(
        data => {
          client.currentUser = Object.freeze({
            id: data.sub,
            user_id: data.user_id,
            email: data.email,
            expiresIn: Number(data.exp),
            role: data.realm_access && data.realm_access.roles || []
          })
          next()
        }
      ).catch(e => {
        next({ data: new ErrorResponse(e) })
      })
    } else {
      next({ data: new ErrorResponse({ ...ResponseCode.TOKEN_NOT_SIGNED }) })
    }
  }
}

module.exports = { Authorization }
