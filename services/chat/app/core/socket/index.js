const socketIO = require('socket.io')
const config = require('../../config')
const { Authorization } = require('./Authorization')
const { MessageResource } = require('../http/resources')
const { MessageModel } = require('../../models')
const { RoleAccess } = require('../../utils')

const adminRoom = 'admin_room'

class Socket {
  io

  constructor ({ server, logger }) {
    logger.info('Socket start initialization...')
    this.io = socketIO(server, { path: '/api/chat' })
    this.init()
  }

  get chatRoomAdmin () {
    return adminRoom
  }

  /**
   * Init socket
   *
   */
  init () {
    this.io.use((client, next) => {
      Authorization.handler(client, next)
    }).on('connection', client => {
      this.chat(client, client.currentUser)
    })

    this.io.of('/dialog').use((client, next) => {
      Authorization.handler(client, next)
    }).on('connection', client => {
      this.chatDialog(client, client.currentUser)
    })
  }

  /**
   * Initial chat for socket
   *
   * @param client
   * @param user
   */
  chat (client, user) {
    const { user_id: userID } = user

    client.on('subscribe', () => {
      client.join(userID, () => {
        MessageModel.getMessages(userID).then(messages => client.emit('messages', messages))
        MessageModel.getUnreadMessages(userID).then(data => client.emit('unread-messages', data && data.count || 0))
      })
    })
    client.on('unsubscribe', () => client.leave(userID))
  }

  /**
   * Initial chat dialog for socket
   *
   * @param client
   * @param user
   */
  chatDialog (client, user) {
    let { user_id: userID } = user

    client.on('subscribe', data => {
      if (data && data.id) {
        client.customID = data.id
        client.join(userID + '_' + data.id, async () => {
          const messageClient = await MessageModel.getMessagesById(userID, data.id)
          client.emit('messages', messageClient)

          const messages = await MessageModel.getMessages(userID)
          const unreadMessage = await MessageModel.getUnreadMessages(userID)

          this.io.to(userID).emit('messages', messages)
          this.io.to(userID).emit('unread-messages', unreadMessage && unreadMessage.count || 0)
        })
      }
    })
    client.on('unsubscribe', data => {
      if (data && data.id) {
        client.customID = null
        client.leave(userID + '_' + data.id)
      }
    })
    client.on('read-message', async data => {
      userID = this.checkRoleUser(user, data) || userID
      client.customID && await this.readMessage(userID, client.customID)
    })
    client.on('create-message', async data => {
      userID = this.checkRoleUser(user, data) || userID
      data && data.content && await this.createMessage(userID, data, client)
    })
  }

  /**
   * Create message for user
   *
   * @param userID
   * @param data
   * @param client
   * @returns {Promise<void>}
   */
  async createMessage (userID, data, client) {
    if (client.customID) {
      const customID = client.customID
      const messageData = await MessageModel.createMessage(userID, { id: customID || '', content: data.content || '' })
      client.emit('message', MessageResource.sender(messageData))
      await this.updateMessage(customID, userID, client)
    }
  }

  /**
   * Read message
   *
   * @param userID
   * @param customID
   */
  async readMessage (userID, customID) {
    await MessageModel.updateMessageRead(customID, userID)
    const messages = await MessageModel.getMessages(userID)
    const unreadMessage = await MessageModel.getUnreadMessages(userID)

    this.io.to(userID).emit('messages', messages)
    this.io.to(userID).emit('unread-messages', unreadMessage && unreadMessage.count || 0)
  }

  /**
   * Updated message
   *
   * @param customID
   * @param userID
   * @param client
   * @returns {Promise<void>}
   */
  async updateMessage (customID, userID, client) {
    const message = await MessageModel.getMessagesByIdFirst(customID, userID)
    const messagesCustomer = await MessageModel.getMessages(customID)
    const unreadMessageCustomer = await MessageModel.getUnreadMessages(customID)
    this.io.to(customID).emit('messages', messagesCustomer)
    this.io.to(customID).emit('unread-messages', unreadMessageCustomer && unreadMessageCustomer.count || 0)

    const messagesUser = await MessageModel.getMessages(userID)
    this.io.to(userID).emit('messages', messagesUser)

    client.to(customID + '_' + userID).emit('message', message)
  }
}

module.exports = { Socket }
