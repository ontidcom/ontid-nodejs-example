const { DateConvert } = require('../../../utils')

class Message {
  /**
   * Sender response
   *
   * @param data
   * @returns object
   */
  static sender (data) {
    return {
      incoming: false,
      content: data.content || '',
      is_read: false,
      created_at: DateConvert.convertToTime(data.created_at)
    }
  }
}

module.exports = { Message }
