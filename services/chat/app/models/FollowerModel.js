const { Model } = require('objection')
const { BaseModel } = require('../core/lib')

class FollowerModel extends BaseModel {
  static get tableName () {
    return 'followers'
  }

  static get relationMappings () {
    const UserModel = require('./UserModel')

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'followers.follower_id',
          to: 'users.id'
        }
      }
    }
  }

  /**
   * Get followers
   *
   * @param id
   * @returns {Promise<*>}
   */
  static async getFollowers (id) {
    return this.query()
      .select('is_read')
      .where('user_id', id)
      .whereRaw('follower_id IN (SELECT user_id FROM followers WHERE follower_id = ?)', id)
      .withGraphFetched('user(defaultSelect).[profile(defaultSelect)]')
      .orderBy('created_at', 'DESC')
  }
}

module.exports = FollowerModel
