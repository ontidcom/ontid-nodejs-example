const { raw, Model } = require('objection')
const { BaseModel } = require('../core/lib')
const logger = require('../core/Logger')

class MessageModel extends BaseModel {
  static get tableName () {
    return 'messages'
  }

  static get relationMappings () {
    const UserModel = require('./UserModel')

    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'messages.id',
          to: 'users.id'
        }
      }
    }
  }

  /**
   * Get messages
   *
   * @param id
   * @returns {Promise<*>}
   */
  static async getMessages (id) {
    const rawInnerJoinSQL = 'INNER JOIN (SELECT LEAST(sender_id, receiver_id) AS sender_id, GREATEST(sender_id, receiver_id) AS receiver_id, MAX(created_at) AS created_at FROM messages GROUP BY LEAST(sender_id, receiver_id), GREATEST(sender_id, receiver_id)) AS msg ON LEAST(messages.sender_id, messages.receiver_id) = msg.sender_id AND GREATEST(messages.sender_id, messages.receiver_id) = msg.receiver_id AND messages.created_at = msg.created_at'
    const rawSQL = raw(`(SELECT (CASE WHEN messages.sender_id = :id THEN messages.receiver_id ELSE messages.sender_id END) AS id, (CASE WHEN messages.sender_id = :id THEN false ELSE true END) AS incoming, messages.content, messages.is_read, INT4(date_part('EPOCH', messages.created_at::timestamp)) AS created_at FROM messages ${rawInnerJoinSQL} WHERE messages.sender_id = :id OR messages.receiver_id = :id) m`, { id })

    return this.query()
      .select('id', raw('(SELECT COUNT(*) FROM messages WHERE receiver_id = m.id AND is_read = false) AS unread'), 'incoming', 'content', 'is_read', 'created_at')
      .orderBy('created_at', 'DESC')
      .from(rawSQL)
      .withGraphFetched('user(defaultSelect).[profile(defaultSelect).[job(selectName).[title(selectName), field(selectName)]]]')
  }

  /**
   * Get unread messages
   *
   * @param id
   * @returns {Promise<*>}
   */
  static async getUnreadMessages (id) {
    const rawInnerJoinSQL = 'INNER JOIN (SELECT LEAST(sender_id, receiver_id) AS sender_id, GREATEST(sender_id, receiver_id) AS receiver_id, max(created_at) AS created_at FROM messages GROUP BY LEAST(sender_id, receiver_id), GREATEST(sender_id, receiver_id)) AS msg ON LEAST(messages.sender_id, messages.receiver_id) = msg.sender_id AND GREATEST(messages.sender_id, messages.receiver_id) = msg.receiver_id AND messages.created_at = msg.created_at'
    const rawSQL = raw(`(SELECT (CASE WHEN messages.sender_id = :id THEN messages.receiver_id else messages.sender_id END) AS id, (CASE WHEN messages.sender_id = :id THEN false ELSE true END) AS incoming, messages.content, messages.is_read, INT4(date_part('EPOCH', messages.created_at::timestamp)) AS created_at FROM messages ${rawInnerJoinSQL} WHERE messages.receiver_id = :id AND messages.is_read = false) m`, { id })

    return this.query()
      .from(rawSQL)
      .count()
      .first()
  }

  /**
   * Get messages by ID
   *
   * @param userId
   * @param id
   * @returns {Promise<*>}
   */
  static async getMessagesById (userId, id) {
    const rawSelectSQL = raw('(CASE WHEN sender_id = :id THEN false ELSE true END) AS incoming, content, is_read, INT4(date_part(\'EPOCH\', created_at::timestamp)) AS created_at', { id: userId })

    await this.updateMessageRead(id, userId)
    return this.query()
      .select(rawSelectSQL)
      .whereRaw('(sender_id = :id or receiver_id = :id)', { id })
      .whereRaw('(sender_id = :id or receiver_id = :id)', { id: userId })
      .orderBy('created_at')
      .limit(500)
      .catch(() => this.errorNotFound())
  }

  /**
   * Get messages by ID first
   *
   * @param userId
   * @param id
   * @returns {Promise<*>}
   */
  static async getMessagesByIdFirst (userId, id) {
    const rawSelectSQL = raw('(CASE WHEN sender_id = :id THEN false ELSE true END) AS incoming, content, is_read, INT4(date_part(\'EPOCH\', created_at::timestamp)) AS created_at', { id: userId })

    return this.query()
      .select(rawSelectSQL)
      .whereRaw('(sender_id = :id or receiver_id = :id)', { id })
      .whereRaw('(sender_id = :id or receiver_id = :id)', { id: userId })
      .orderBy('created_at', 'DESC')
      .first()
      .catch(() => this.errorNotFound())
  }

  /**
   * Create message
   *
   * @param userID
   * @param params
   * @returns {Promise<*>}
   */
  static async createMessage (userID, params) {
    return this.query()
      .insert({ sender_id: userID, receiver_id: params.id, content: params.content })
      .returning('*')
      .catch(e => {
        logger.error('MessageModel - createMessage', e)
        return this.errorDB()
      })
  }

  /**
   * Updated message read
   *
   * @param senderID
   * @param receiverID
   * @returns {Promise<*>}
   */
  static async updateMessageRead (senderID, receiverID) {
    return this.query()
      .update({ is_read: true })
      .where('sender_id', senderID)
      .where('receiver_id', receiverID)
  }
}

module.exports = MessageModel
