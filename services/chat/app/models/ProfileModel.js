const { raw, Model } = require('objection')
const { BaseModel } = require('../core/lib')

class ProfileModel extends BaseModel {
  static get tableName () {
    return 'profiles'
  }

  static modifiers = {
    defaultSelect (query) {
      query.select('avatar', 'first_name', 'last_name')
    }
  }
}

module.exports = ProfileModel
