const { Model } = require('objection')
const { BaseModel } = require('../core/lib')
const logger = require('../core/Logger')

class UserModel extends BaseModel {
  static get tableName () {
    return 'users'
  }

  static modifiers = {
    defaultSelect (query) {
      query.select('id', 'phone', 'email', 'fake')
    }
  }

  static get relationMappings () {
    const ProfileModel = require('./ProfileModel')

    return {
      profile: {
        relation: Model.BelongsToOneRelation,
        modelClass: ProfileModel,
        join: {
          from: 'users.profile_id',
          to: 'profiles.id'
        }
      }
    }
  }

  /**
   * Get one user by Id
   *
   * @param id
   * @returns {Promise<*>}
   */
  static async getOneById (id) {
    const data = this.query()
      .findById(id)
      .withGraphFetched('profile(defaultSelect)')
      .select('users.id', 'users.phone', 'users.email')
      .catch(e => {
        logger.error('UserModel - getOneById', e)
        return this.errorNotFound()
      })

    return !data ? this.errorNotFound() : data
  }
}

module.exports = UserModel
