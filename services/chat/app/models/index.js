const MessageModel = require('./MessageModel')
const FollowerModel = require('./FollowerModel')

module.exports = {
  MessageModel,
  FollowerModel
}
