class RoleAccess {
  /**
   * Access
   */
  static access (user, role) {
    const roleActivate = (e) => role.indexOf(e) !== -1

    return user && user.role && user.role.some(roleActivate)
  }
}

module.exports = { RoleAccess }
