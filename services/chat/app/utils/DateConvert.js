class DateConvert {
  /**
   * Convert to time - unix Timestamp
   *
   * @param param
   * @returns {number}
   */
  static convertToTime (param) {
    let time = new Date(param).getTime()
    if (isNaN(time)) {
      time = new Date().getTime()
    }

    return time / 1000 | 0
  }
}

module.exports = { DateConvert }
