const path = require('path')
const fs = require('fs')
const jsonWebToken = require('jsonwebtoken')
const ResponseCode = require('../core/lib/ResponseCode')
const { AppError } = require('../core/lib/AppError')
const logger = require('../core/Logger')

const publicKEY = fs.readFileSync(path.join(__dirname, '..', './../key/public.key'), 'utf8')

class JWT {
  /**
   * Verify
   *
   * @param token
   */
  static async verify (token) {
    return new Promise((resolve, reject) => {
      jsonWebToken.verify(token, publicKEY, (error, decoded) => {
        if (error) {
          logger.error('JWT', error)
          if (error.name === 'TokenExpiredError') {
            reject(new AppError({ ...ResponseCode.TOKEN_EXPIRED }))
          }
          reject(new AppError({ ...ResponseCode.TOKEN_VERIFY }))
        }
        resolve(decoded)
      })
    })
  }
}

module.exports = { JWT }
