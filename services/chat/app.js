const { Model } = require('objection')
const knex = require('knex')
const { Server } = require('./app/server')
const config = require('./app/config')
const { Assert: assert } = require('./app/core/lib/assert')
const middleware = require('./app/core/middleware')
const errorMiddleware = require('./app/core/middleware/error')
const logger = require('./app/core/Logger')

new Server({
  port: Number(config.app.port),
  middleware,
  errorMiddleware,
  logger
}).then(serverParams => {
  logger.info('Server initialized...', serverParams)
  logger.trace('Configs')
  logger.trace('App config:', config.app)
}).catch(error => logger.error('Server fails to initialize...', error))
  .then(() => Model.knex(knex(config.knex)))
  .then(() => testDbConnection(knex(config.knex)))
  .then(() => {
    logger.trace('--- Database ---')
    logger.trace('Database initialized...', config.knex)
  }).catch(error => {
    logger.error('Database fails to initialize...', error)
    process.exit(1)
  })

async function testDbConnection (instance) {
  assert.func(instance, { required: true })
  assert.func(instance.raw, { required: true })

  try {
    await instance.raw('select 1+1 as result')
  } catch (e) {
    throw e
  } finally {
    instance.destroy()
  }
}
